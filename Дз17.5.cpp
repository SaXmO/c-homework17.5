﻿#include <iostream>
#include <cmath>
class Vector
{
public:
    Vector() : x(2), y(3), z(6)
    {}
    Vector (double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << "\n";
    }
    void modul()
    {
       std::cout <<  sqrt(pow(x,2) + pow(y,2) + pow(z,2));
    }
private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v;

    v.Show();
    v.modul();
}